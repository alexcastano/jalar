ExUnit.start()

defmodule TestHelper do
  def fake_stream(content \\ "file") do
    {:ok, io} = StringIO.open(content)
    IO.binstream(io, 5 * 1024 * 1024)
  end
end
