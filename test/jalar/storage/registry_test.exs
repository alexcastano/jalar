defmodule Jalar.Storage.RegistryTest do
  use ExUnit.Case, async: true

  alias Jalar.Storage.Registry, as: Subject

  # setup do
  #   conf = %{"cache" => {Jalar.Storage.FileSystem, directory: "/tmp/jalar_registry_test"}}

  #   Application.put_env(@app, @storages_key, conf)

  #   {:ok, pid} = Registry.start_link(otp_application: @app, storages_key: @storages_key)
  #     {:ok, pid: pid}
  # end

  test "works" do
    assert nil == Subject.lookup("testing")

    assert :ok = Subject.insert("testing", %Jalar.Storage.FileSystem{directory: "/tmp/jalar_registry_test"})

    assert %Jalar.Storage.FileSystem{directory: "/tmp/jalar_registry_test"} = Subject.lookup("testing")

    assert :ok = Subject.delete("testing")

    assert nil == Subject.lookup("testing")
  end

  test "load from config" do
    assert Subject.lookup("cache")
    refute Subject.lookup("non_existed")
  end
end
