defmodule Olyseum.Uploads.S3StorageTest do
  use ExUnit.Case

  alias Jalar.S3Storage

  def s3(opts \\ []) do
    [ bucket: "my-bucket" ]
    |> Keyword.merge(opts)
    |> S3Storage.new()
  end

  describe "new" do
    test "raises an error when :bucket is nil" do
      assert_raise(ArgumentError, fn -> S3Storage.new([]) end)
    end
  end
  # test "valid_url?" do
  #   # valid
  #   url =  "https://s3-eu-west-1.amazonaws.com/test-dev-api-olyseum/test-cache/test.jpg"
  #   assert S3Storage.valid_url?(@storage, url)

  #   # no bucket in path
  #   url =  "https://s3-eu-west-1.amazonaws.com/test-cache/test.jpg"
  #   refute S3Storage.valid_url?(@storage, url)

  #   # no prefix in path
  #   url =  "https://s3-eu-west-1.amazonaws.com/test-dev-api-olyseum/test.jpg"
  #   refute S3Storage.valid_url?(@storage, url)

  #   # no id in path
  #   url =  "https://s3-eu-west-1.amazonaws.com/test-dev-api-olyseum/test-cache/"
  #   refute S3Storage.valid_url?(@storage, url)

  #   # no id in path
  #   url =  "https://s3-eu-west-1.amazonaws.com/test-dev-api-olyseum/test-cache"
  #   refute S3Storage.valid_url?(@storage, url)

  #   # storage no prefix
  #   url =  "https://s3-eu-west-1.amazonaws.com/test-dev-api-olyseum/test-cache/test.jpg"
  #   assert S3Storage.valid_url?(%{@storage | prefix: nil}, url)

  #   # external url
  #   url = "https://google.es/image.jpg"
  #   refute S3Storage.valid_url?(@storage, url)

  # end

  # test "url works" do
  #   url =  "https://s3-eu-west-1.amazonaws.com/test-dev-api-olyseum/test-cache/test.jpg"
  #   assert ^url = S3Storage.url(@storage, "test.jpg")
  # end

  # @tag :skip
  # test "download/2 returns error with invalid url" do
  #   url = "https://google.es/image.jpg"
  #   {:error, :invalid_host} = S3Storage.download(@storage, url)
  # end

  # test "upload/4 returns error with invalid file" do
  #   assert {:error, :enoent} = S3Storage.upload(@storage, "test", "not_a_file", [])
  # end

  # @tag :api
  # test "full cycle works" do
  #   id = "test_file.jpg"
  #   S3Storage.delete(@storage, id)

  #   refute S3Storage.exists?(@storage, id)

  #   metadata = [content_type: "image/jpeg", acl: :public_read]
  #   assert S3Storage.upload(@storage, id, "test/fixtures/test.jpg", metadata)

  #   assert S3Storage.exists?(@storage, id)

  #   {:ok, tmp} = S3Storage.download(@storage, id)
  #   assert File.exists?(tmp)

  #   assert S3Storage.delete(@storage, id)
  #   refute S3Storage.exists?(@storage, id)
  # end
end
