defmodule Jalar.Storage.FileSystemTest do
  use ExUnit.Case, async: true

  import TestHelper
  alias Jalar.Storage.FileSystem, as: Subject

  def assert_permissions(expected, path) do
    use Bitwise, only_operators: true
    assert expected == (File.lstat!(path).mode &&& 0o777)
  end

  def file_system(dir, opts \\ []), do: Subject.new(Keyword.put(opts, :directory, dir))

  def random_string(),
    do: :crypto.strong_rand_bytes(16) |> Base.url_encode64() |> binary_part(0, 16)

  def generate_root(), do: Path.join([System.tmp_dir(), "jalar", random_string()])

  setup do
    root = generate_root()
    on_exit(fn -> File.rm_rf(root) end)

    %{root: root}
  end

  describe "new" do
    test "raises an error when :directory is nil" do
      assert_raise(ArgumentError, fn -> Subject.new([]) end)
    end

    test "expands the directory", %{root: root} do
      assert Path.expand(root) == file_system(root).directory
      assert Path.expand("#{root}/prefix") == file_system(root, prefix: "prefix").directory
    end

    test "creates the given directory", %{root: root} do
      file_system(root)
      assert File.dir?(root)

      file_system(root, prefix: "uploads")
      assert File.dir?("#{root}/uploads")
    end

    test "sets directory permissions", %{root: root} do
      file_system(root, directory_permissions: 0o777)
      assert_permissions(0o777, root)
    end

    test "doesn't change permissions of existing directories", %{root: root} do
      File.mkdir!(root)
      File.chmod!(root, 0o777)
      file_system(root)
      assert_permissions(0o777, root)
    end

    test "handles directory permissions being nil", %{root: root} do
      file_system(root, directory_permissions: nil)
    end
  end

  describe "upload" do
    test "creates subdirectories", %{root: root} do
      storage = file_system(root)

      assert {:ok, %UploadedFile{}} = Subject.upload(storage, fake_stream(), "a/a/a.jpg")
      assert Subject.exists?(storage, "a/a/a.jpg")
    end

    test "copies full file content", %{root: root} do
      storage = file_system(root)

      content = String.duplicate("A", 20_000)

      dst = Subject.upload(storage, stream, "foo.jpg")
      assert 20_000 == File.stat!(dst).size
    end

    test "sets file permissions", %{root: root} do
      dst_path =
        file_system(root, permissions: 0o600)
        |> Subject.upload(fake_stream(), "foo")

      assert_permissions(0o600, dst_path)
    end

    #   test "sets directory permissions on intermediary directories" do
    #     # TODO
    #   end
  end

  describe "open" do
    test "works with a valid file", %{root: root} do
      storage = file_system(root)
      Subject.upload(storage, fake_stream(), "foo")
      assert {:ok, _} = Subject.open(storage, "foo")
    end

    test "returns :error with a invalid file", %{root: root} do
      storage = file_system(root)
      assert {:error, :enoent} = Subject.open(storage, "foo")
    end
  end

  describe "#delete" do
    test "cleans  subdirectories", %{root: root} do
      storage = file_system(root)
      Subject.upload(storage, fake_stream(), "a/a/a.jpg")
      Subject.delete(storage, "a/a/a.jpg")
      refute Subject.exists?(storage, "a/a/a.jpg")
      refute Subject.exists?(storage, "a/a")
    end
  end

  describe "#url" do
    test "returns the full path without :prefix", %{root: root} do
      storage = file_system(root)
      assert "#{root}/foo.jpg" == Subject.url(storage, "foo.jpg")
    end

    test "applies a host without :prefix", %{root: root} do
      storage = file_system(root)
      host = "http://124.83.12.24"
      assert "#{host}#{root}/foo.jpg" == Subject.url(storage, "foo.jpg", host: host)
    end

    test "returns the path relative to the :prefix", %{root: root} do
      storage = file_system(root, prefix: "uploads")
      assert "/uploads/foo.jpg" == Subject.url(storage, "foo.jpg")
    end

    test "applies a host with :prefix", %{root: root} do
      storage = file_system(root, prefix: "uploads")
      host = "http://abc123.cloudfront.net"
      assert "#{host}/uploads/foo.jpg" == Subject.url(storage, "foo.jpg", host: host)
    end
  end

  describe "#clear!" do
    test "can purge the whole directory", %{root: root} do
      storage = file_system(root)
      assert :ok = Subject.clear!(storage)
      assert %{type: :directory} = File.stat!(root)
    end

    test "reestablishes directory permissions", %{root: root} do
      storage = file_system(root, directory_permissions: 0o777)
      assert :ok = Subject.clear!(storage)
      assert_permissions(0o777, root)
    end
  end

  describe "#path" do
    test "returns path to the file", %{root: root} do
      storage = file_system(root, prefix: "uploads")
      assert "#{root}/uploads/foo/bar/baz" == Subject.path(storage, "foo/bar/baz")
    end
  end

  describe "#clean" do
    test "deletes empty directories up the hierarchy", %{root: root} do
      storage = file_system(root)
      Subject.upload(storage, fake_stream(), "a/a/a/a.jpg")
      Subject.delete(storage, "a/a/a/a.jpg")
      refute Subject.exists?(storage, "a")
      assert File.exists?(root)

      Subject.upload(storage, fake_stream(), "a/a/a/a.jpg")
      Subject.upload(storage, fake_stream(), "a/b.jpg")
      Subject.delete(storage, "a/a/a/a.jpg")
      refute Subject.exists?(storage, "a/a")
      assert Subject.exists?(storage, "a")
    end
  end
end
