defmodule Jalar.UploaderTest do
  use ExUnit.Case, async: true

  import TestHelper
  alias Jalar.Uploader, as: Subject
  alias Jalar.UploadedFile

  describe "upload/2" do
    test "stores the file" do
      uploaded_file = Subject.upload(fake_stream("original"))
      assert UploadedFile.exists?(uploaded_file)
      assert UploadedFile.read(uploaded_file) == "original"
    end
  end
end
