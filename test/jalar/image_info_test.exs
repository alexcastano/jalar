defmodule Jalar.ImageInfoTest do
  use ExUnit.Case

  alias Jalar.ImageInfo

  test "returns error with non-existing file" do
    assert {:error, :enoent} = ImageInfo.new("this_is_not_a_file")
  end

  test "works with jpg" do
    path = "./test/fixtures/test.jpg"
    assert {:ok, info} = ImageInfo.new(path)
    assert %{
      uri: ^path,
      height: 67,
      width: 100,
      size: 6147,
      recommended_extension: ".jpg",
      mime_type: "image/jpeg",
    } = info
  end

  test "works with png" do
    path = "./test/fixtures/test.png"
    assert {:ok, info} = ImageInfo.new(path)
    assert %{
      uri: ^path,
      height: 128,
      width: 128,
      size: 2683,
      recommended_extension: ".jpg",
      mime_type: "image/png",
    } = info
  end
end
