defmodule Jalar.ExternalExecutionTest do
  use ExUnit.Case

  alias Jalar.ExternalExecution

  test "exectable_exists? works" do
    assert ExternalExecution.executable_exists?("ls")
    refute ExternalExecution.executable_exists?("thisnotexist")
  end

  describe "apply" do
    test "works with string args" do
      assert {:ok, "mix.exs\n"} = ExternalExecution.apply("ls", "mix.exs")
      assert {:ok, "mix.exs\n"} = ExternalExecution.apply("ls", "-1 mix.exs")
    end

    test "works with list args" do
      assert {:ok, "mix.exs\n"} = ExternalExecution.apply("ls", ["mix.exs"])
      assert {:ok, "mix.exs\n"} = ExternalExecution.apply("ls", ["-1", "mix.exs"])
      assert {:ok, "mix.exs\n"} = ExternalExecution.apply("ls", ~w(-1 mix.exs))
    end

    test "returns errors" do
      assert {:error, _error} = ExternalExecution.apply("ls", "non_existing_file")
    end
  end
end
