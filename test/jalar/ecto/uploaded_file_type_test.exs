defmodule Jalar.Ecto.UploadedFileTypeTest do
  use ExUnit.Case

  alias Jalar.Ecto.{UploadedFileType}
  alias Jalar.{UploadedFile}

  describe "cast" do
    test "works with atom keys" do
      input = %{id: "id", storage: "storage", metadata: %{}}
      assert {:ok, %UploadedFile{} = file} = UploadedFileType.cast(input)
      assert file.id == "id"
      assert file.storage == :storage
      assert file.metadata == %{}
    end

    test "works with string keys" do
      input = %{"id" => "id", "storage" => "storage", "metadata" => %{}}
      assert {:ok, %UploadedFile{} = file} = UploadedFileType.cast(input)
      assert file.id == "id"
      assert file.storage == :storage
      assert file.metadata == %{}
    end

    test "works without metadata" do
      input = %{id: "id", storage: "storage"}
      assert {:ok, %UploadedFile{metadata: %{}}} = UploadedFileType.cast(input)
    end

    test "return error with invalid input" do
      assert :error = UploadedFileType.cast(nil)
      assert :error = UploadedFileType.cast(%{})
      assert :error = UploadedFileType.cast(%{id: "id"})
      assert :error = UploadedFileType.cast(%{id: nil, storage: nil})
      assert :error = UploadedFileType.cast(%{id: nil, storage: "storage"})
    end
  end

  describe "load" do
    test "works" do
      input = %{"id" => "id", "storage" => "storage", "metadata" => %{"meta" => "meta"}}
      assert {:ok, %UploadedFile{} = file} = UploadedFileType.load(input)
      assert file.id == "id"
      assert file.storage == :storage
      assert file.metadata == %{"meta" => "meta"}
    end
  end

  describe "dump" do
    test "works with UploadedFile" do
      input = %UploadedFile{id: "id", storage: "storage", metadata: %{"meta" => "meta"}}
      assert {:ok, %{
        id: "id",
        storage: "storage",
        metadata: %{"meta" => "meta"}
      }} == UploadedFileType.dump(input)
    end
  end
end
