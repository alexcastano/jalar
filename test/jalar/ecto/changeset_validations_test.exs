defmodule Jalar.Ecto.ChangesetValidationsTest do
  use ExUnit.Case

  # alias Ecto.Changeset
  # import Jalar.Ecto.ChangesetValidations

  # test "validate simple urls" do
  #   url =  "https://s3-eu-west-1.amazonaws.com/test-dev-api-olyseum/test-cache/test.jpg"
  #   changeset = Changeset.change({%{}, %{url: :string}}, url: url)
  #   assert %Changeset{valid?: true} =
  #     validate_url(changeset, :url, :cache)

  #   url =  "https://s3-eu-west-1.amazonaws.com/test-cache/test.jpg"
  #   changeset = Changeset.change({%{}, %{url: :string}}, url: url)
  #   assert %Changeset{
  #     valid?: false,
  #     errors: [url: {"Invalid bucket", []}],
  #   } = validate_url(changeset, :url, :cache)

  #   url =  "https://s3-eu-west-1.amazonaws.com/test-dev-api-olyseum/test.jpg"
  #   changeset = Changeset.change({%{}, %{url: :string}}, url: url)
  #   assert %Changeset{
  #     valid?: false,
  #     errors: [url: {"Invalid prefix", []}],
  #   } = validate_url(changeset, :url, :cache)

  #   url =  "https://s3-eu-west-1.amazonaws.com/test-dev-api-olyseum/test-cache/"
  #   changeset = Changeset.change({%{}, %{url: :string}}, url: url)
  #   assert %Changeset{
  #     valid?: false,
  #     errors: [url: {"Invalid id", []}],
  #   } = validate_url(changeset, :url, :cache)

  #   url = "https://google.es/image.jpg"
  #   changeset = Changeset.change({%{}, %{url: :string}}, url: url)
  #   assert %Changeset{
  #     valid?: false,
  #     errors: [url: {"Invalid host", []}],
  #   } = validate_url(changeset, :url, :cache)
  # end

  # test "validate url list" do
  #   url =  "https://s3-eu-west-1.amazonaws.com/test-dev-api-olyseum/test-cache/test.jpg"
  #   changeset = Changeset.change({%{}, %{urls: {:array, :string}}}, urls: [url])
  #   assert %Changeset{valid?: true} =
  #     validate_url(changeset, :urls, :cache)

  #   url =  "https://s3-eu-west-1.amazonaws.com/test-cache/test.jpg"
  #   changeset = Changeset.change({%{}, %{urls: {:array, :string}}}, urls: [url])
  #   assert %Changeset{
  #     valid?: false,
  #     errors: [urls: {"0: Invalid bucket", []}],
  #   } = validate_url(changeset, :urls, :cache)

  #   changeset = Changeset.change({%{}, %{urls: {:array, :string}}}, urls: [url, url])
  #   assert %Changeset{
  #     valid?: false,
  #     errors: [urls: {"1: Invalid bucket", []},
  #              urls: {"0: Invalid bucket", []}],
  #   } = validate_url(changeset, :urls, :cache)
  # end
end
