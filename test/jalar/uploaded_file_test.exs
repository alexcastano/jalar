defmodule Jalar.UploadedFileTest do
  use ExUnit.Case

  alias Jalar.UploadedFile

  describe "new" do
    test "with valid data works" do
      metadata = %{any: :thing}
      assert {:ok, %UploadedFile{
        id: "asdf",
        storage: :cache,
        metadata: ^metadata,
      } } = UploadedFile.new(%{
        id: "asdf",
        storage: "cache",
        metadata: metadata,
      })
    end

    test "with invaild data returns :error" do
      assert :error = UploadedFile.new(%{id: "id"})
      assert :error = UploadedFile.new(%{storage: "storage"})
      assert :error = UploadedFile.new(%{})
    end

    test "with string keys works" do
      metadata = %{"any" => "thing"}
      assert {:ok, %UploadedFile{
        id: "asdf",
        storage: :cache,
        metadata: ^metadata,
      } } = UploadedFile.new(%{
        "id" => "asdf",
        "storage" => "cache",
        "metadata" => metadata,
      })
    end
  end

  def uploaded_file(data \\ %{}) do
    %{id: "foo", storage: :cache, metadata: %{}}
    |> Map.merge(data)
    |> UploadedFile.new()
    |> elem(1)
  end

  describe "id" do
    test "is fetched from data" do
      assert "foo" == uploaded_file(%{id: "foo"}) |> UploadedFile.id()
    end
  end

  describe "storage_key" do
    test "is fetched from data" do
      assert :cache == uploaded_file(%{storage_key: :cache}) |> UploadedFile.storage_key()
    end
  end

  describe "metadata" do
    test "is fetched from data" do
      metadata = %{"foo" => "foo"}
      assert metadata == uploaded_file(%{metadata: metadata}) |> UploadedFile.metadata()
    end
  end

  describe "original_filename" do
    test "returns filename from metadata" do
      assert "foo.jpg" == %{metadata: %{"filename" => "foo.jpg"}}
                          |> uploaded_file()
                          |> UploadedFile.original_filename()

      assert nil == %{metadata: %{"filename" => nil}}
                    |> uploaded_file()
                    |> UploadedFile.original_filename()

      assert nil == uploaded_file() |> UploadedFile.original_filename()
    end
  end

end
