defmodule Jalar.UtilsTest do
  use ExUnit.Case

  alias Jalar.Utils

  describe "generate_temporary_path" do
    test "works" do
      assert Regex.match?(~r(^\/tmp\/.*\.jpg$),
                         Utils.generate_temporary_path(".jpg"))
    end
  end
end
