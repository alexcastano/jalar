defmodule Jalar.MixProject do
  use Mix.Project

  def project do
    [
      app: :jalar,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Jalar.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_aws, "~> 2.0"},
      {:ex_aws_s3, "~> 2.0"},
      {:ex_image_info, "~> 0.2.0"},
      {:ecto, "~> 2.2.8"},
      {:hackney, "~> 1.11.0"},
      {:sweet_xml, "~> 0.6.5"},
    ]
  end
end
