defmodule Jalar do
  alias Jalar.UploadedFile

  def url(%{storage: storage_name, id: id}, opts \\ %{}), do: apply_storage(storage_name, :url, [id, opts])

  def get_storage!(name) do
    {module, data} =
      Application.fetch_env!(:jalar, :storages)
      |> Map.fetch!(name)
      |> Map.pop(:type)

    struct(module, data)
  end

  def apply_storage(name, method, args) do
    args = List.wrap(args)
    storage = get_storage!(name)
    apply(storage.__struct__, method, [storage | args])
  end

  def format_error({:no_storage, name}),
    do: "#{inspect name} is not a storage"

  defmodule Error do
    defexception [:reason]

    def exception(reason), do: %__MODULE__{reason: reason}
    def message(reason), do: Jalar.format_error(reason)
  end
end
