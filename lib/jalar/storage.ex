defmodule Jalar.Storage do
  alias Jalar.UploadedFile

  @type error() :: {:error, any}
  @type storage() :: struct
  @type file() :: any
  @type id() :: String.t()
  @type opts() :: keyword

  @callback url(storage, id, opts) :: String.t()
  @callback download(storage, id, opts) :: {:ok, String.t()} | error
  @callback upload(storage, id, file, opts) :: {:ok, String.t()} | error
  @callback delete(storage, id, opts) :: :ok | error
  @callback exists?(storage, id, opts) :: boolean
  @callback open(storage, id, opts) :: {:ok, pid} | error
  @callback clear!(storage, opts) :: :ok
end

# defprotocol Jalar.Storage do

#   @type opts :: Keyword.t

#   @spec url(t, String.t, opts) :: String.t
#   def url(storage, id, opts)

#   @spec download(t, String.t, opts) :: {:ok, String.t} | {:error, term}
#   def download(storage, id, opts)

#   @spec upload(t, String.t, String.t, opts) :: {:ok, String.t} | {:error, term}
#   def upload(storage, id, file, upload_opts)

#   @spec delete(t, String.t, opts) :: :ok | {:error, term}
#   def delete(storage, id, opts)

#   @spec exists?(t, id :: String.t, opts :: opts) :: boolean
#   def exists?(storage, id, opts)

#   @spec open(t, id :: String.t, opts :: opts) :: {:ok, pid} | {:error, term}
#   def open(storage, id, opts)

#   @spec clear!(t, opts :: opts) :: :ok
#   def clear!(storage, opts)
# end

# defimpl Jalar.Storage, for: Any do
#   def url(storage, id, opts \\ []), do: @for.url(storage, id, opts)
#   def download(storage, id, opts \\ []), do: @for.download(storage, id, opts)
#   def upload(storage, id, file, opts \\ []),
#     do: @for.upload(storage, id, file, opts)
#   def delete(storage, id, opts \\ []), do: @for.delete(storage, id, opts)
#   def exists?(storage, id, opts \\ []), do: @for.exists?(storage, id, opts)
#   def open(storage, id, opts \\ []), do: @for.open(storage, id, opts)
#   def clear!(storage, opts \\ []), do: @for.clear!(storage, opts)
# end
