defprotocol Jalar.Signature.Protocol do
  @spec signature(t, String.t, Keyword.t) :: Jalar.Signature.t
  def signature(storage, id, opts)
end

defimpl Jalar.Signature.Protocol, for: Any do
  def signature(storage, id, opts \\ []), do: @for.signature(storage, id, opts)
end
