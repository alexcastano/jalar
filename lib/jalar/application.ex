defmodule Jalar.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    Supervisor.start_link([Jalar.Storage.Registry], strategy: :one_for_one)
  end
end
