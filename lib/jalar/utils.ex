defmodule Jalar.Utils do
  def generate_temporary_path(extension \\ "") do
    file_name =
      20
      |> :crypto.strong_rand_bytes()
      |> Base.encode32()
      |> Kernel.<>(extension)

    Path.join(System.tmp_dir, file_name)
  end
end

