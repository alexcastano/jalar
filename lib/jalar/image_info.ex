defmodule Jalar.ImageInfo do
  alias __MODULE__

  defstruct [:uri, :mime_type, :recommended_extension, :size, :width, :height]

  def new(filepath) when is_binary(filepath) do
    with {:ok, image_info} <- map(filepath),
    do: {:ok, struct(ImageInfo, image_info)}
  end

  def map(filepath) when is_binary(filepath) do
    with {:ok, size} <- calc_size(filepath),
      {mime_type, width, height, _} <- extract_image_info(filepath),
      extension <- guess_extension(mime_type)
    do
      {:ok, %{
        uri: filepath,
        mime_type: mime_type,
        recommended_extension: extension,
        width: width,
        height: height,
        size: size,
      }}
    end
  end

  def map!(filepath) do
    case map(filepath) do
      {:ok, ret} -> ret
      {:error, error} -> raise error
      _ -> raise "Unexpected error"
    end
  end

  defp guess_extension(mime_type) do
    case mime_type do
      {"image/gif", _} -> ".gif"
      {"image/jpeg", _} -> ".jpg"
      {"image/png", _} -> ".png"
      {"image/webp", _} -> ".webp"
      _ -> ".jpg"
    end
  end

  defp calc_size(filepath) do
    with {:ok, stat} <- File.stat(filepath), do: {:ok, stat.size}
  end

  defp extract_image_info(filepath) do
    case  _extract_image_info(filepath) do
      nil -> {:error, :unknown_image}
      ret -> ret
    end
  end

  defp _extract_image_info(filepath) do
    filepath
    |> File.read!()
    |> ExImageInfo.info()
  end
end
