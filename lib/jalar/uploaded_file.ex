defmodule Jalar.UploadedFile do
  alias __MODULE__

  @type t :: %__MODULE__{
          id: String.t(),
          storage: any,
          metadata: map
        }
  @enforce_keys [:id, :storage]
  defstruct id: nil, storage: nil, metadata: %{}

  def new(id, storage, metadata \\ %{}) do
    new(%{id: id, storage: storage, metadata: metadata})
  end

  def new(%__MODULE__{} = p), do: {:ok, p}

  def new(%{id: id, storage: %{} = storage} = map) when is_binary(id) do
    {:ok, struct(__MODULE__, map)}
  end

  def new(_), do: :error

  @doc """
  The location where the file was uploaded to the storage.
  """
  @spec id(t) :: String.t()
  def id(%UploadedFile{id: id}), do: id

  @doc """
  The storage where the file is uploaded to.
  """
  @spec storage(t) :: term
  def storage(%UploadedFile{storage: storage}), do: storage

  @doc """
  A hash of file metadata that was extracted during upload.
  """
  @spec metadata(t) :: map
  def metadata(%UploadedFile{metadata: metadata}), do: metadata

  @doc """
  The filename that was extracted from the uploaded file.
  """
  @spec original_filename(t) :: map
  def original_filename(%UploadedFile{metadata: %{"filename" => filename}}),
    do: filename

  def original_filename(%UploadedFile{}), do: nil

  @doc """
  The filesize of the uploaded file.
  """
  def size(%UploadedFile{metadata: %{"size" => size}}) do
    to_integer(size)
  end

  def size(%UploadedFile{}), do: nil

  defp to_integer(nil), do: nil
  defp to_integer(size) when is_binary(size), do: String.to_integer(size)
  defp to_integer(size) when is_integer(size), do: size

  @doc """
  The MIME type of the uploaded file.
  """
  def mime_type(%UploadedFile{metadata: %{"mime_type" => mime_type}}), do: mime_type
  def mime_type(%UploadedFile{}), do: nil

  def exists?(%UploadedFile{} = file, opts \\ []),
    do: apply_storage(file, :exists?, [opts])

  def url(%UploadedFile{} = file, opts \\ []),
    do: apply_storage(file, :url, [opts])

  def upload(%UploadedFile{} = file, local_path, opts \\ %{}),
    do: apply_storage(file, :upload, [local_path, opts])

  def download(%UploadedFile{} = file, opts \\ []),
    do: apply_storage(file, :download, [opts])

  def delete(%UploadedFile{} = file, opts \\ []),
    do: apply_storage(file, :delete, [opts])

  def signature(%UploadedFile{} = file, opts \\ []),
    do: apply_storage(file, :signature, [opts])

  defp apply_storage(%UploadedFile{storage: storage, id: id}, method, args) do
    apply(storage.__struct__, method, [storage, id | args])
  end
end
