defprotocol Jalar.Signature do
  @enforce_keys [:url, :fields]
  defstruct [:url, :fields, headers: %{}]

  @type t :: %__MODULE__{}
end
