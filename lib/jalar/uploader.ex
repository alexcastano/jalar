defmodule Jalar.Uploader do
  # The symbol identifier for the storage used by the uploader.
  @storage_key "store"
  @registry Jalar.Storage.Registry

  alias Jalar.UploadedFile

  @doc """
  The storage struct used by the uploader.
  """
  def storage(context \\ %{})
  def storage(%{storage: store}), do: get_storage(store)
  def storage(_), do: get_storage(@storage_key)

  defp get_storage(storage_key) when is_atom(storage_key),
    do: @registry.lookup(@storage_key)

  defp get_storage(%{__struct__: _} = storage), do: storage

  @doc """
  The main method for uploading files.
  Takes a filepath and an
  optional context hash (used internally by Jalar::Attacher). It calls
  user-defined process/2, and afterwards it calls store/2.
  """
  def upload(filepath, context \\ %{}) do
    filepath = process(filepath, context) || filepath
    store(filepath, context)
  end

  @doc """
  User is expected to perform processing inside this method, and
  return the processed files. Returning nil signals that no proccessing
  has been done and that the original file should be used.

      defmodule ImageUploader do
      use Jalar.Uploader
        def process(filepath, context)
          # do processing and return processed files
        end
      end
  """
  def process(filepath, context \\ %{}), do: nil

  @doc """
  Uploads the file and returns an instance of Jalar::UploadedFile. By
  default the location of the file is automatically generated by
  generate_location/2, but you can pass in `:location` to upload to
  a specific location.

      uploader.store(filepath)
  """
  def store(filepath, context \\ %{}) do
    _store(filepath, context)
  end

  @doc """
  Returns true if the storage of the given uploaded file matches the
  storage of this uploader.
  """
  def uploaded?(uploaded_file) do
    uploaded_file.storage_key == @storage_key
  end

  @doc """
  Deletes the given uploaded file and returns it.
  """
  def delete(uploaded_file, context \\ %{}) do
    _delete(uploaded_file, context)
    uploaded_file
  end

  @doc """
  Generates a unique location for the uploaded file, preserving the
  file extension. Can be overriden in uploaders for generating custom
  location.
  """
  def generate_location(io, context \\ %{}) do
    "#{generate_uid(io)}#{Jalar.Uploadable.extension(io)}"
  end

  @doc """
  Extracts filename, size and MIME type from the file, which is later
  accessible through UploadedFile#metadata.
  """
  def extract_metadata(io, context \\ %{}) do
    %{
      "filename" => extract_filename(io),
      "size" => extract_size(io),
      "mime_type" => extract_mime_type(io)
    }
  end

  # Attempts to extract the appropriate filename from the IO object.
  defp extract_filename(io), do: Jalar.Uploadable.filename(io)

  # Attempts to extract the MIME type from the IO object.
  defp extract_mime_type(io), do: Jalar.Uploadable.mime_type(io)

  # Extracts the filesize from the IO object.
  defp extract_size(io), do: Jalar.Uploadable.size(io)

  # It extracts
  # metadata and generates the location, before calling the storage to
  # upload the file, passing the extracted metadata and location.
  # Finally it returns a Jalar.UploadedFile object which represents the
  # file that was uploaded.
  defp _store(filepath, context) do
    metadata = extract_metadata(filepath, context)
    context = Map.put(context, :metadata, metadata)
    location = get_location!(filepath, context)
    context = Map.put(context, :location, location)

    copy(filepath, context)

    %UploadedFile{
      id: location,
      storage: storage(),
      metadata: metadata
    }
  end

  # Delegates to #remove.
  defp _delete(uploaded_file, context) do
    remove(uploaded_file, context)
  end

  # Calls `#upload` on the storage, passing to it the location, metadata
  # and any upload options.
  # The storage might modify the location or metadata that were passed in.
  defp copy(filepath, %{location: location, metadata: metadata} = context) do
    upload_options = Map.get(context, :upload_options, %{})

    opts = Map.put(upload_options, :shrine_metadata, metadata)
    s = storage(context)
    s.__struct__.upload(s, filepath, location, opts)
  end

  # Delegates to `UploadedFile#delete`.
  defp remove(uploaded_file, context) do
    uploaded_file.delete
  end

  # Retrieves the location for the given IO and context. First it looks
  # for the `:location` option, otherwise it calls #generate_location.
  defp get_location!(io, context) do
    if location = Map.get(context, :location, generate_location(io, context)),
      do: location,
      # TODO improve error
      else: raise("location generated for #{inspect(io)} was nil (context = #{inspect(context)})")
  end

  # Generates a unique identifier that can be used for a location.
  defp generate_uid(_io) do
    16
    |> :crypto.strong_rand_bytes()
    |> Base.hex_encode32()
    |> binary_part(0, 16)
  end
end

# An possible future abstraction to avoid pass only a filepath
defmodule Jalar.Uploadable do
  @moduledoc false
  alias Jalar.UploadedFile

  def extension(s) do
    if filename = filename(s) do
      filename
      |> Path.extname()
      |> String.downcase()
    end
  end

  def filename(filepath) when is_binary(filepath),
    do: "#{Path.basename(path)}#{Path.extname(path)}"

  def filename(%File.Stream{path: path}), do: filename(path)
  def filename(%UploadedFile{} = f), do: UploadedFile.original_filename(f)
  def filename(_), do: nil

  # TODO
  # warn("The \"mime_type\" Shrine metadata field will be set from the \"Content-Type\" request header, which might not hold the actual MIME type of the file. It is recommended to load the determine_mime_type plugin which determines MIME type from file content.")
  def mime_type(%UploadedFile{} = f), do: UploadedFile.mime_type(f)
  def mime_type(_), do: nil

  def size(filepath) when is_binary(filepath) do
    filepath
    |> File.stat()
    |> case do
      {:ok, %File.Stat{size: size}} -> size
      _ -> nil
    end
  end

  def size(%File.Stream{path: path}), do: size(path)

  def size(%UploadedFile{} = f), do: UploadedFile.size(f)
  def size(_), do: nil

  # def open_stream(%UploadedFile{}), do: nil
  # def open_stream(s) when is_pid(s), do: IO.binstream(s, 5 * 1024 * 1024)
  # def open_stream(%File.Stream{} = s), do: s
  # def open_stream(%IO.Stream{} = s), do: s
  # def open_stream(t), do: t

  # def close_stream(%UploadedFile{}), do: nil
  # def close_stream(s, _) when is_pid(s), do: :ok
  # def close_stream(%File.Stream{}, _), do: :ok
  # def close_stream(%IO.Stream{}, _), do: :ok
  # def close_stream(t, _), do: :ok
end
