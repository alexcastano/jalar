defmodule Jalar.ExternalExecution do
  def apply(program, args) do
    case System.cmd(program, args_list(args), stderr_to_stdout: true) do
      {output, 0} -> {:ok, output}
      {error_message, _exit_code} -> {:error, error_message}
    end
  end

  def executable_exists?(program) do
    case System.find_executable(program) do
      nil -> false
      _ -> true
    end
  end

  defp args_list(args) when is_list(args), do: args
  defp args_list(args), do: ~w(#{args})
end
