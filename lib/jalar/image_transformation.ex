defmodule Jalar.ImageTransformation do
  require ExImageInfo
  alias Jalar.{Utils, ImageInfo, ExternalExecution}

  @executable "convert"

  def verify_executable() do
    ExternalExecution.executable_exists?(@executable)
  end

  def thumbnail(file, size) do
    thumbnail(file, size, size)
  end

  def thumbnail(%ImageInfo{} = image_info, width, height) do
    args = thumbnail_args(width, height)
    exec(image_info, args)
  end

  def thumbnail(filepath, width, height) when is_binary(filepath) do
    with {:ok, image_info} <- ImageInfo.new(filepath),
    do: thumbnail(image_info, width, height)
  end

  def resize(filename, size), do: resize(filename, size, size)
  def resize(%ImageInfo{} = filename, width, height) do
    args = resize_args(width, height)
    exec(filename, args)
  end

  def resize(filepath, width, height) do
    resize(ImageInfo.new(filepath), width, height)
  end

  defp thumbnail_args(width, height) do
    ~w(-strip -thumbnail #{width}x#{height}>)
  end

  defp resize_args(width, height) do
    ~w(-strip -resize #{width}x#{height})
  end

  defp convert_to(mime_type) do
    case mime_type do
      {"image/jpeg", _} -> ".jpg"
      {"image/png", _} -> ".png"
      {"image/gif", _} -> ".png"
      _ -> ".jpg"
    end
  end

  defp exec(image_info, args) do
    extension = convert_to(image_info.mime_type)
    output = Utils.generate_temporary_path(extension)
    args = [image_info.uri] ++ args ++ [output]

    case ExternalExecution.apply(@executable, args) do
      {:ok, _} -> {:ok, output}
      ret -> ret
    end
  end
end
