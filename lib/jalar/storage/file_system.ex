defmodule Jalar.Storage.FileSystem do

  @behaviour Jalar.Storage

  @enforce_keys [:directory]
  defstruct directory: nil,
            host: nil,
            prefix: nil,
            permissions: nil,
            directory_permissions: nil,
            clean?: true

  @type t :: %__MODULE__{}

  @doc """
  Initializes a storage for uploading to the filesystem.

  :host
  :  The host used to build the url. It can also be added as argument
  to the url function.

  :prefix
  :  The directory relative to `directory` to which files will be stored,
  and it is included in the URL.

  :permissions
  :  The UNIX permissions applied to created files. Can be set to `nil`,
  in which case the default permissions will be applied. Defaults to
  `0644`.

  :directory_permissions
  :  The UNIX permissions applied to created directories. Can be set to
  `nil`, in which case the default permissions will be applied. Defaults
  to `0755`.

  :clean?
  :  By default empty folders inside the directory are automatically
  deleted, but if it happens that it causes too much load on the
  filesystem, you can set this option to `false`.
  """
  def new(opts) do
    directory =
      opts[:directory] || raise ArgumentError, message: "directory is a mandatory option"

    prefix = opts[:prefix] |> relative()

    abs_directory =
      [directory, prefix]
      |> Enum.reject(&is_nil/1)
      |> Path.join()
      |> Path.expand()

    unless File.exists?(abs_directory) do
      File.mkdir_p!(abs_directory)

      if opts[:directory_permissions],
        do: File.chmod!(abs_directory, opts[:directory_permissions])
    end

    %__MODULE__{
      directory: abs_directory,
      prefix: prefix,
      permissions: opts[:permissions],
      directory_permissions: opts[:directory_permissions],
      clean?: opts[:clean?] || true
    }
  end

  defp relative("/" <> prefix), do: prefix
  defp relative(prefix), do: prefix

  @doc """
  If #prefix is not present, returns a path composed of #directory and
  the given `id`. If #prefix is present, it excludes the #directory part
  from the returned path (e.g. #directory can be set to "public" folder).
  Both cases accept a `:host` value which will be prefixed to the
  generated path.
  """
  def url(storage, id, opts \\ [])

  def url(%__MODULE__{prefix: nil, directory: directory, host: storage_host}, id, opts) do
    url = Path.join(directory, id)
    host = Keyword.get(opts, :host, storage_host)
    insert_host_to_url(url, host)
  end

  def url(%__MODULE__{prefix: prefix, host: storage_host}, id, opts) do
    url = Path.join(["/", prefix, id])
    host = Keyword.get(opts, :host, storage_host)

    insert_host_to_url(url, host)
  end

  defp insert_host_to_url(url, nil), do: url
  defp insert_host_to_url(url, host), do: Path.join(host, url)

  def download(%__MODULE__{prefix: prefix, host: storage_host}, id, _opts \\ []) do
  end

  @doc """
  Copies the file into the given location.
  """
  def upload(storage, input_stream, id, opts \\ [])

  def upload(%__MODULE__{permissions: permissions} = storage, input_stream, id, _opts) do
    dst_path = path!(storage, id)

    bytes = 5 * 1024 * 1024
    output_stream = File.stream!(dst_path, [:delayed_write], bytes)

    input_stream |> Stream.into(output_stream) |> Stream.run()

    if permissions, do: File.chmod!(dst_path, permissions)

    dst_path
  end

  @doc """
  Delets the file, and by default deletes the containing directory if
  it's empty.
  """
  def delete(%__MODULE__{clean?: clean?} = storage, id) do
    dst_path = path(storage, id)

    File.rm(dst_path)
    |> case do
      :ok ->
        if clean?, do: clean(storage, dst_path)
        :ok

      err ->
        err
    end
  end

  @doc """
  Returns true if the file exists on the filesystem.
  """
  def exists?(%__MODULE__{} = storage, id, _opts \\ []) do
    storage
    |> path(id)
    |> File.exists?()
  end

  @doc """
  """
  def open(%__MODULE__{} = storage, id) do
    storage
    |> path(id)
    |> File.open([:binary, :read])
  end

  @doc """
  Returns the full path to the file.
  """
  def path(%__MODULE__{directory: directory}, id) do
    Path.expand(id, directory)
  end

  @doc """
  Creates all intermediate directories for that location.
  Returns the full path to the file.
  """
  def path!(%__MODULE__{} = storage, id) do
    %{directory_permissions: perms, directory: directory} = storage

    directory
    |> Path.join(id)
    |> Path.dirname()
    |> File.mkdir_p!()

    if perms do
      # TODO permissions
    end

    path(storage, id)
  end

  @doc """
  Deletes all files from the #directory. If `:older_than` is passed in (a
  `Time` object), deletes all files which were last modified before that
  time.
  """
  def clear!(%__MODULE__{directory: directory, directory_permissions: perms}, opts \\ []) do
    opts
    |> Keyword.get(:older_than)
    |> if do
      # TODO older_than
    else
      File.rm_rf!(directory)
      File.mkdir_p!(directory)
      if perms, do: File.chmod(directory, perms)
    end

    :ok
  end

  @doc """
  Cleans all empty subdirectories up the hierarchy.
  """
  def clean(%__MODULE__{directory: directory}, path) do
    do_clean(directory, Path.dirname(path))
  end

  defp do_clean(directory, directory), do: nil

  defp do_clean(directory, path) do
    with true <- empty_dir?(path),
         :ok <- File.rmdir(path),
         ascend = Path.expand([path, "/.."]) do
      do_clean(directory, ascend)
    end
  end

  defp empty_dir?(path), do: path |> File.ls!() |> Enum.empty?()
end
