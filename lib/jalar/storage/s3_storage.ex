#defmodule Jalar.S3Storage do
#  @moduledoc """
#  The S3 storage handles uploads to Amazon S3 service, using the
#  [ex_aws_s3] package:

#      {:ex_aws, "~> 2.0"},
#      {:ex_aws_s3, "~> 2.0"},

#  It is initialized with the following 4 required options:

#  s3 = Jalar::S3Storage.new(
#    access_key_id: "abc",
#    secret_access_key: "xyz",
#    region: "eu-west-1",
#    bucket: "my-app",
#  )

#  `access_key_id`, `secret_access_key` and `region` are optional
#  if they are already configured in ex_aws.

#  ## Prefix
  
#  The `:prefix` option can be specified for uploading all files inside
#  a specific S3 prefix (folder), which is useful when using S3 for both
#  cache and store:
  
#      Jalar::S3Storage.new(prefix: "cache", **s3_options)
#      Jalar::S3Storage.new(prefix: "store", **s3_options)
  
#  ## CDN
  
#  If you're using a CDN with S3 like Amazon CloudFront, you can specify
#  the `:host` option to `#url`:
  
#      StorageS3.url(s3, "image.jpg", host: "http://abc123.cloudfront.net")
#      #=> "http://abc123.cloudfront.net/image.jpg"
  
#  ## Accelerate endpoint
  
#  To use Amazon S3's [Transfer Acceleration] feature, you can change the
#  `:endpoint` of the underlying client to the accelerate endpoint, and this
#  will be applied both to regular and presigned uploads, as well as
#  download URLs.
  
#      Jalar::S3Storage.new(endpoint: "https://s3-accelerate.amazonaws.com")
  
#  ## Presigns
  
#  This storage can generate presigns for direct uploads to Amazon S3, and
#  it accepts additional options which are passed to ex_aws_s3. There are
#  two places in which you can specify presign options:
  
#  * in `:upload_options` option on this storage
#  * in `Storage::S3#presign` by forwarding options
  
#  ## Large files
  
#  The aws-sdk-s3 gem has the ability to automatically use multipart
#  upload/copy for larger files, splitting the file into multiple chunks
#  and uploading/copying them in parallel.
  
#  By default any files that are uploaded will use the multipart upload
#  if they're larger than 15MB, and any files that are copied will use the
#  multipart copy if they're larger than 150MB, but you can change the
#  thresholds via `:multipart_threshold`.
  
#      thresholds = {upload: 30*1024*1024, copy: 200*1024*1024}
#      Shrine::Storage::S3.new(multipart_threshold: thresholds, **s3_options)
  
#  If you want to change how many threads aws-sdk-s3 will use for multipart
#  upload/copy, you can use the `upload_options` plugin to specify
#  `:thread_count`.
  
#      plugin :upload_options, store: ->(io, context) do
#        {thread_count: 5}
#      end
  
#  ## Clearing cache
  
#  If you're using S3 as a cache, you will probably want to periodically
#  delete old files which aren't used anymore. S3 has a built-in way to do
#  this, read [this article][object lifecycle] for instructions.
  
#  Alternatively you can periodically call the `#clear!` method:
  
#      # deletes all objects that were uploaded more than 7 days ago
#      s3.clear! { |object| object.last_modified < Time.now - 7*24*60*60 }
  
#  [uploading]: http://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/S3/Object.html#put-instance_method
#  [copying]: http://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/S3/Object.html#copy_from-instance_method
#  [presigning]: http://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/S3/Object.html#presigned_post-instance_method
#  [aws-sdk-s3]: https://github.com/aws/aws-sdk-ruby/tree/master/gems/aws-sdk-s3
#  [Transfer Acceleration]: http://docs.aws.amazon.com/AmazonS3/latest/dev/transfer-acceleration.html
#  [object lifecycle]: http://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/S3/Object.html#put-instance_method
#  """

#  @doc """
#  Initializes a storage for uploading to S3.
  
#  :access_key_id
#  :secret_access_key
#  :region
#  :bucket
#  :   Credentials required by the `aws-sdk-s3` gem.
  
#  :prefix
#  :   "Folder" name inside the bucket to store files into.
  
#  :upload_options
#  :   Additional options that will be used for uploading files, they will
#      be passed to [`Aws::S3::Object#put`], [`Aws::S3::Object#copy_from`]
#      and [`Aws::S3::Bucket#presigned_post`].
  
#  :multipart_threshold
#  :   If the input file is larger than the specified size, a parallelized
#      multipart will be used for the upload/copy. Defaults to
#      `{upload: 15*1024*1024, copy: 100*1024*1024}` (15MB for upload
#      requests, 100MB for copy requests).
  
#  All other options are forwarded to [`Aws::S3::Client#initialize`].
  
#  [`Aws::S3::Object#put`]: http://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/S3/Object.html#put-instance_method
#  [`Aws::S3::Object#copy_from`]: http://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/S3/Object.html#copy_from-instance_method
#  [`Aws::S3::Bucket#presigned_post`]: http://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/S3/Object.html#presigned_post-instance_method
#  [`Aws::S3::Client#initialize`]: http://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/S3/Client.html#initialize-instance_method
#  """

#  # alias Jalar.{S3Storage, Utils}

#  # @enforce_keys [:bucket]
#  # defstruct [:bucket, :prefix, :host, :ex_aws_opts, :multipart_threshold]

#  # @type t :: %__MODULE__{}

#  # def new(opts) do
#  #   bucket = opts[:bucket] || raise ArgumentError, message: "bucket is a mandatory option"
#  #   multipart_threshold = Map.merge(%{upload: 15*1024*1024, copy: 100*1024*1024}, opts[:multipart_threshold] || %{})
#  #   %__MODULE__{
#  #     ex_aws_opts: opts[:ex_aws_opts] || %{},
#  #     # upload_options: opts[:upload_options] || %{},
#  #     bucket: bucket,
#  #     multipart_threshold: multipart_threshold,
#  #   }
#  # end

#  # def url(%S3Storage{} = storage, id, _opts \\ []) do
#  #   storage
#  #   |> build_uri(id)
#  #   |> URI.to_string()
#  # end

#  # def download(%S3Storage{} = storage, id, _opts \\ []) do
#  #   with download_path = Utils.generate_temporary_path(),
#  #        aws_path = prepend_prefix(storage, id),
#  #        aws_conf = ex_aws_config(storage),
#  #        {:ok, :done} <-
#  #          ExAws.S3.download_file(storage.bucket, aws_path, download_path)
#  #          |> ExAws.request(aws_conf) do
#  #            {:ok, download_path}
#  #          end
#  # end

#  # def download!(%S3Storage{} = storage, url) do
#  #   case download(storage, url) do
#  #     {:ok, download_path} -> download_path
#  #     {:error, error} -> raise error
#  #   end
#  # end

#  # @doc """
#  # If the file is an UploadedFile from S3, issues a COPY command, otherwise
#  # uploads the file. For files larger than `:multipart_threshold` a
#  # multipart upload/copy will be used for better performance and more
#  # resilient uploads.

#  # It assigns the correct "Content-Type" taken from the MIME type, because
#  # by default S3 sets everything to "application/octet-stream".
#  # """
#  # def upload(%S3Storage{} = storage, id, file_path, upload_opts \\ []) do
#  #   if File.exists?(file_path) do
#  #     do_upload(storage, id, file_path, upload_opts)
#  #   else
#  #     {:error, :enoent}
#  #   end
#  # end

#  # defp do_upload(%S3Storage{} = storage, id, file_path, upload_opts) do
#  #   remote_path = prepend_prefix(storage, id)
#  #   aws_conf = ex_aws_config(storage)

#  #   file_path
#  #   |> ExAws.S3.Upload.stream_file()
#  #   |> ExAws.S3.upload(storage.bucket, remote_path, upload_opts)
#  #   |> ExAws.request(aws_conf)
#  # end

#  # def upload!(%S3Storage{} = storage, filepath, remote_path, content_type) do
#  #   case upload(storage, filepath, remote_path, content_type) do
#  #     :done -> :ok
#  #     {:ok, _} -> :ok
#  #     {:error, error} -> raise error
#  #   end
#  # end

#  # def delete(%S3Storage{} = storage, remote_path, _opts \\ []) do
#  #   remote_path = prepend_prefix(storage, remote_path)
#  #   aws_conf = ex_aws_config(storage)

#  #   storage.bucket
#  #   |> ExAws.S3.delete_object(remote_path)
#  #   |> ExAws.request(aws_conf)
#  # end

#  # def delete!(%S3Storage{} = storage, remote_path, _opts \\ []) do
#  #   case delete(storage, remote_path) do
#  #     :done -> :ok
#  #     {:error, error} -> raise error
#  #   end
#  # end

#  # def exists?(%S3Storage{} = storage, remote_path, _opts \\ []) do
#  #   remote_path = prepend_prefix(storage, remote_path)
#  #   aws_conf = ex_aws_config(storage)

#  #   case storage.bucket
#  #   |> ExAws.S3.head_object(remote_path)
#  #   |> ExAws.request(aws_conf) do
#  #     {:ok, _} -> true
#  #     {:error, _} -> false
#  #   end
#  # end

#  # def valid_url?(%S3Storage{} = storage, remote_url) do
#  #   case id_from_url(storage, remote_url) do
#  #     {:ok, _} -> true
#  #     _ -> false
#  #   end
#  # end

#  # def url_error(%S3Storage{} = storage, remote_url) do
#  #   case id_from_url(storage, remote_url) do
#  #     {:ok, _} -> :ok
#  #     ret -> ret
#  #   end
#  # end

#  # def id_from_url(%S3Storage{host: host} = storage, url) do
#  #   case URI.parse(url) do
#  #     %URI{
#  #       scheme: "https",
#  #       host: ^host,
#  #       path: path
#  #     } ->
#  #       id_from_full_path(storage, path)

#  #     _ ->
#  #       {:error, :invalid_host}
#  #   end
#  # end

#  # defp id_from_full_path(%S3Storage{} = storage, "/" <> full_path),
#  # do: id_from_full_path(storage, String.split(full_path, "/"))

#  # defp id_from_full_path(%S3Storage{bucket: bucket} = storage, [bucket | prefixed_path]),
#  # do: id_from_prefixed_path(storage, prefixed_path)

#  # defp id_from_full_path(%S3Storage{}, _), do: {:error, :invalid_bucket}

#  # defp id_from_prefixed_path(%S3Storage{prefix: nil}, prefixed_path) when is_list(prefixed_path),
#  # do: build_id(prefixed_path)

#  # defp id_from_prefixed_path(%S3Storage{prefix: prefix}, [prefix | path_list]),
#  # do: build_id(path_list)

#  # defp id_from_prefixed_path(%S3Storage{}, _), do: {:error, :invalid_prefix}

#  # defp build_id(path_list) when is_list(path_list) do
#  #   if path_list == [] || path_list == [""],
#  #   do: {:error, :invalid_id},
#  #   else: {:ok, Path.join(path_list)}
#  # end

#  # defp build_uri(%S3Storage{} = storage, path \\ nil) do
#  #   %URI{
#  #     scheme: "https",
#  #     host: storage.host,
#  #     path: Path.join(["/", storage.bucket || "", storage.prefix || "", path || ""])
#  #   }
#  # end

#  # defp ex_aws_config(%S3Storage{} = storage) do
#  #   Map.take(storage, [:access_key_id, :secret_access_key, :bucket])
#  # end

#  # defp request(request, %S3Storage{} = storage) do

#  # end

#  # defp prepend_prefix(%S3Storage{} = storage, remote_path) do
#  #   Path.join(["/", storage.prefix || "", remote_path])
#  # end

#  # def signature(%S3Storage{} = storage, id, opts \\ []) do
#  #   Jalar.S3Storage.Signature.signature(storage, id, opts)
#  # end
#end
