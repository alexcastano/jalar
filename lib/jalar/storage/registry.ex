defmodule Jalar.Storage.Registry do
  @moduledoc false

  use GenServer

  ## Public interface

  def start_link(opts \\ []) do
    otp_application = Keyword.get(opts, :otp_application, :jalar)
    storages_key = Keyword.get(opts, :key, :storages)
    GenServer.start_link(__MODULE__, {otp_application, storages_key}, name: __MODULE__)
  end

  def insert(name, storage) when is_binary(name) do
    GenServer.call(__MODULE__, {:insert, name, storage})
  end

  def delete(name) when is_binary(name) do
    GenServer.call(__MODULE__, {:delete, name})
  end

  def lookup(name) when is_binary(name) do
    # :ets.lookup_element(__MODULE__, name, 2)
    :ets.lookup(__MODULE__, name)
    |> case do
      [] -> nil
      [{^name, storage}] -> storage
    end
  end

  ## Callbacks

  @impl true
  def init({otp_app, storages_key}) do
    table = :ets.new(__MODULE__, [:named_table, read_concurrency: true])

    load_storages(otp_app, storages_key)
    |> Enum.each(fn {name, storage} -> insert_storage(table, name, storage) end)

    {:ok, table}
  end

  defp load_storages(otp_app, key) do
    Application.get_env(otp_app, key, [])
    |> Enum.map(fn {name, {type, data}} -> {name, struct!(type, data)} end)
  end

  @impl true
  def handle_call({:insert, name, storage}, _from, table) do
    insert_storage(table, name, storage)
    {:reply, :ok, table}
  end

  @impl true
  def handle_call({:delete, name}, _from, table) do
    delete_storage(table, name)
    {:reply, :ok, table}
  end

  defp insert_storage(table, name, storage) do
    # TODO init storages?
    # TODO check repeated names
    true = :ets.insert(table, {name, storage})
  end

  defp delete_storage(table, name) do
    # TODO terminate storages?
    :ets.delete(table, name)
  end
end
