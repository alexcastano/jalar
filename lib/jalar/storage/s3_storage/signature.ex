# defmodule Jalar.S3Storage.Signature do

#   alias Jalar.S3Storage

#   def signature(%S3Storage{}=storage, key, opts \\ []) do
#     key = prefix_key(storage, key)
#     policy = policy(storage, key, opts)

#     %Jalar.Signature{
#       url: url(storage, key, opts),
#       fields: %{
#         key: key,
#         policy: policy,
#         acl: acl(storage, key, opts),
#         success_action_status: success_action_status(storage, key, opts),
#         "x-amz-credential": credential(storage),
#         "x-amz-algorithm": "AWS4-HMAC-SHA256",
#         "x-amz-date": "#{date_string()}T000000Z",
#         "x-amz-signature": sign(storage, policy),
#       },
#       headers: %{}
#     }
#   end

#   defp prefix_key(%{prefix: prefix}, key), do: Path.join([prefix || "", key])

#   defp acl(_storage, _key, opts), do: opts[:acl] || "private"
#   defp success_action_status(_storage, _key, opts), do: opts[:success_action_status] || "201"

#   defp url(%{region: region, bucket: bucket}, _key, _opts),
#     do: "https://#{bucket}.s3.#{region}.amazonaws.com"

#   defp credential(%{access_key_id: access_key_id, region: region}) do
#     "#{access_key_id}/#{date_string()}/#{region}/s3/aws4_request"
#   end

#   defp now_plus(minutes) do
#     NaiveDateTime.utc_now
#     |> NaiveDateTime.add(minutes * 60)
#     |> NaiveDateTime.to_iso8601()
#     |> Kernel.<>("Z")
#   end

#   defp policy(storage, key, opts) do
#     expiration_window = opts[:expiration_window] || 60
#     %{
#       expiration: now_plus(expiration_window),
#       conditions: conditions(storage, key, opts)
#     }
#     |> Poison.encode!()
#     |> Base.encode64()
#   end

#   defp conditions(%{bucket: bucket}=storage, key, opts) do
#     [
#       %{bucket: bucket},
#       %{acl: acl(storage, key, opts)},
#       %{key: key},
#       %{success_action_status: success_action_status(storage, key, opts)},
#       %{'x-amz-algorithm': "AWS4-HMAC-SHA256"},
#       %{'x-amz-credential': credential(storage)},
#       %{'x-amz-date': date_string() <> "T000000Z"},
#     ] |> Kernel.++(opts[:additional_conditions] || [])
#   end

#   defp sign(%{secret_access_key: secret_access_key, region: region}, policy) do
#     "AWS4#{secret_access_key}"
#     |> hmac_sha256(date_string())
#     |> hmac_sha256(region)
#     |> hmac_sha256("s3")
#     |> hmac_sha256("aws4_request")
#     |> hmac_sha256(policy)
#     |> Base.encode16(case: :lower)
#   end

#   defp hmac_sha256(key, data) do
#     :crypto.hmac(:sha256, key, data)
#   end

#   defp date_string(date \\ Date.utc_today) do
#     date
#     |> Date.to_iso8601()
#     |> String.replace("-", "")
#   end
# end
