# defmodule Jalar.TestStorage do
#   alias __MODULE__

#   @derive [Jalar.Storage]

#   defstruct [ret: :default_ret]

#   def url(%TestStorage{ret: :default_ret}, id, _opts), do: id
#   def url(%TestStorage{ret: ret}, _id, _opts), do: ret

#   def download(%TestStorage{ret: :default_ret}, id, _opts), do: {:ok, "test/fixtures/test.jpg"}
#   def download(%TestStorage{ret: ret}, _id, _opts), do: ret

#   def upload(%TestStorage{ret: :default_ret}, _filepath, remote_path, _opts),
#     do: {:ok, remote_path}
#   def upload(%TestStorage{ret: ret}, _filepath, _remote_path, _opts), do: ret

#   def delete(%TestStorage{ret: :default_ret}, _id, _opts), do: :ok
#   def delete(%TestStorage{ret: ret}, _id, _opts), do: ret

#   def exists?(%TestStorage{ret: :default_ret}, _id, _opts), do: true
#   def exists?(%TestStorage{ret: ret}, _id, _opts), do: ret

#   def signature(%TestStorage{}, id, _opts \\ []),
#     do: %Jalar.Signature{url: id, fields: %{}}


#   def apply_return_value_to_storage(name, value \\ :default_ret) do
#     storages = Application.get_env(:olyseum, :storages)
#     storages = put_in(storages, [name, :ret], value)
#     Application.put_env(:olyseum, :storages, storages)
#   end
# end
