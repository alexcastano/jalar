defmodule Jalar.Ecto.UploadedFileType do
  @behaviour Ecto.Type

  alias Jalar.UploadedFile

  def type, do: :map

  def cast(%UploadedFile{} = input), do: {:ok, input}
  def cast(input = %{}) do
    with {:ok, id} <- get(input, :id),
         {:ok, storage} <- get(input, :storage),
         {:ok, storage} <- atomize(storage),
         metadata = get(input, :metadata, %{})
    do
        {:ok, %UploadedFile{
          id: id,
          storage: storage,
          metadata: metadata
        }}
    end
  end
  def cast(_), do: :error

  def load(%{"id" => id, "storage" => storage, "metadata" => metadata}) do
    with {:ok, storage} <- atomize(storage) do
      {:ok, %UploadedFile{
        id: id,
        storage: storage,
        metadata: metadata,
      }}
    end
  end

  def dump(%UploadedFile{} = file), do: {:ok, Map.from_struct(file)}
  def dump(_), do: :error

  defp atomize(nil), do: :error
  defp atomize(v) when is_atom(v), do: {:ok, v}
  defp atomize(v) when is_binary(v), do: {:ok, String.to_existing_atom(v)}
  defp atomize(_), do: :error

  defp get(map, key, default) do
    case get(map, key) do
      {:ok, value} -> value
      :error -> default
    end
  end

  defp get(map, key) do
    case Map.fetch(map, key) do
      {:ok, nil} -> :error
      {:ok, value} -> {:ok, value}
      :error -> Map.fetch(map, Atom.to_string(key))
    end
  end
end
