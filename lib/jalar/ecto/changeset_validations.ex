defmodule Jalar.Ecto.ChangesetValidations do
  alias Jalar.S3Storage

  def validate_url(changeset, field, storage_name) do
    storage = Jalar.get_storage!(storage_name)
    Ecto.Changeset.validate_change(changeset, field,
                                   &_validate_url(storage, &1, &2))
  end

  defp _validate_url(storage, field, urls) when is_list(urls) do
    urls
    |> Stream.with_index()
    |> Enum.reduce([], fn({url, index}, acc) ->
      case _validate_url(storage, field, url) do
        [{field, message}] -> [{field, "#{index}: #{message}"} | acc]
        [] -> acc
      end
    end)
  end

  defp _validate_url(storage, field, url) when is_binary(url) do
    case S3Storage.url_error(storage, url) do
      :ok -> []
      {:error, error} -> [{field, error_message(error)}]
    end
  end

  def error_message(:invalid_host), do: "Invalid host"
  def error_message(:invalid_bucket), do: "Invalid bucket"
  def error_message(:invalid_prefix), do: "Invalid prefix"
  def error_message(:invalid_id), do: "Invalid id"
end
