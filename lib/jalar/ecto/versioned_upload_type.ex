defmodule Jalar.Ecto.VersionedUploadType do
  @behaviour Ecto.Type
  alias Jalar.UploadedFile

  def type, do: :map

  def cast(%{} = input) do
    input
    |> Map.to_list()
    |> cast()
  end

  def cast(input) when is_list(input) do
    do_cast(%{}, input)
  end

  def cast(_), do: :error


  defp do_cast(map, [head | rest]) do
    case cast_pair(head) do
      :error -> :error
      {key, uploaded_file} ->
        map
        |> Map.put(key, uploaded_file)
        |> do_cast(rest)
    end
  end

  defp do_cast(map, []), do: {:ok, map}

  defp cast_pair({key, map}) when is_binary(key) do
    cast_pair({String.to_atom(key), map})
  end

  defp cast_pair({key, %{} = map}) when is_atom(key) do
    case UploadedFile.new(map) do
      {:ok, uploaded_file } -> {key, uploaded_file}
      :error -> :error
    end
  end

  defp cast_pair(_), do: :error

  def load(map = %{}), do: cast(map)

  def dump(%{} = input) do
    case cast(input) do
      {:ok, map} -> {:ok, do_dump(%{}, Map.to_list(map))}
      :error -> :error
    end
  end

  def dump(_), do: :error

  defp do_dump(map, [head | rest]) do
    map
    |> dump_pair(head)
    |> do_dump(rest)
  end

  defp do_dump(map, []), do: map

  defp dump_pair(acc, {key, uploaded_file}) do
    map = Map.take(uploaded_file, [:id, :storage, :metadata])
    Map.put(acc, key, map)
  end
end
